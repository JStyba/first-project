import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sda-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent {
@Input() data: string = 'default component data';
border: string = '5px solid pink';
defaultStyle = {
  backgroundColor: 'red',
  border: this.border,
}
customStyle = {
  backgroundColor: 'blue',
  border: '5px dotted black',
}


superhero = 'Iron-man';
//  BASIC STUFF
stringVariable: string = 'This text is amazing';
numberVariable: number = 123.04;
booleanVariable: boolean = true;
arrayOfString: string[] = ['one', 'two', 'three'];
arrayOfString2: Array<string> = ['four', 'five', 'six'];
variableNull = null;
variableUndefined: unknown = undefined;
someVariable: any;
arrayOfUnknown: unknown[] = [1, 'two', true];
objectVariable: object | Person = {name: 'John', age: 30};
objectVariable2: {name: string, age: number} = {name: 'John', age: 30};
objectVariable3: Person = {name: 'John', age: 30};
arrayOfObjectsVariable: Array<Person | object> = [{name: 'John', age: 30, gender: 'unknown'}, this.objectVariable];

private youCannotTouchItInHTML = 'text you can\'t use in html'

// ngOnInit(): void {
//     setTimeout(() => {
//       this.booleanVariable = !this.booleanVariable;
//       console.log('booleanVariable: ', this.booleanVariable);
//     }, 2000);
// }

functionNotDoingMuch(): void { // this function will not return anything
let a: number = 4;
a = a + 1;
const b: number = 7;
const c: number[] = [1, 2, 3];
// b = b + 1; // this will not work
c.forEach(el => {
  console.log(el);
});
}

}


export interface Person {
  name: string;
  age: number;
  gender?: string; //optional
}