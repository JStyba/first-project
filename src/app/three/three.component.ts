import { Component, OnInit } from '@angular/core';
import { ModuloPipe } from '../modulo.pipe';

@Component({
  selector: 'sda-three',
  templateUrl: './three.component.html',
  styleUrls: ['./three.component.scss']
})
export class ThreeComponent implements OnInit {
date = new Date();
  constructor(private modulo: ModuloPipe) { }

  ngOnInit(): void {
    this.modulo.transform(10, 3);
  }

}
