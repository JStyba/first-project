import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sda-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.scss']
})
export class TwoComponent implements OnInit {
  arrayOfString: string[] = ['one', 'two', 'three'];
  objectVariable: any = {name: 'Bob', age: 45, occupation: 'developer'};
  arrayOfObjectsVariable: any[] = [{name: 'John', age: 30, gender: null}, this.objectVariable];
  
  constructor() { }

  ngOnInit(): void {
  }

}

export interface Person {
  name: string;
  age: number;
  gender?: string; //optional
  occupation?: string; //optional
}
