import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[sdaChangeColor]'
})
export class ChangeColorDirective {
@Input() sdaChangeColor: string;
  constructor(private ref: ElementRef, private renderer: Renderer2) {
    
    
   }

   @HostListener('mouseenter')
   onMouseEnter(): void {
     this.changeColor(this.sdaChangeColor)
   }

    @HostListener('mouseleave')
    onMouseLeave(): void {
      this.changeColor('white')
    }

    changeColor(color: string): void {
      this.renderer.setStyle(this.ref.nativeElement,'background-color',color)
    }

}
